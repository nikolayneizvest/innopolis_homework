import json
import asyncio
import aiohttp
import requests
import re
""""Написать программу запроса погоды \
    1.Создать класс для хранения параметров погоды.\
    2.Открыть файл cities.txt сназванием городов, считать города из агрузить в программу список городов.\
    3.По считанным городам параллельно (асинхронно) запросить на openweatherAPI информацию о погоде."""
class Weather():      #Создаем класс для хранения параметров погоды
    def __init__(self, city, humidity, pressure, temp, wind):
        self.city = city
        self.humidity = humidity
        self.pressure = pressure
        self.temp = temp
        self.wind = wind

async def openWeather(session, city):  #запрос на openweatherAPI информацию о погоде
    api_key ='0fb610dab7456bc44dbdde2ddba9be71'
    url = requests.get(f'https://api.openweathermap.org/data/2.5/weather?q={city}&appid={api_key}&units=metric')
    data = json.loads(url.text)
    try:
        city = data['name']
        humidity = data['main']['humidity']
        pressure = data['main']['pressure']
        temp = data['main']['temp']
        wind = data['wind']['speed']
    except KeyError:
        print(f"ошибка для  {city}")
        return None
    return Weather(city, humidity, pressure, temp, wind)
async def main(): #считываем города из файла cities.txt
    cities = []
    with open('cities.txt', 'r') as file:
        data = file.read()
        matches = re.findall(r'[A-Z\s]+', data, flags=re.I)
        for match in matches:
            stripped_match = match.strip()
            if stripped_match != '':
                cities.append(stripped_match)
    async with aiohttp.ClientSession() as session:
        task = []
        for city in cities:
            task.append(asyncio.create_task(openWeather(session, city)))

    weather_data = await asyncio.gather(*task)
    for data in weather_data:
        print(f'В городе {data.city} \n'
                f'Температура воздуха составит: {data.temp} C° \n'
                f'Влажность воздуха: {data.humidity}%\n'
                f'Давление: {data.pressure} мм.р.т.\n'
                f'Скорость ветра: {data.wind}м/с\n')

if __name__ == "__main__":
    asyncio.run(main())
"""asyncio на Python использует ключевые слова async и await для определения асинхронных функций и asyncio.gather()
для одновременного запуска нескольких сопрограмм и ожидания их результатов. В этом случае программа выполняется в одном цикле событий
с использованием asyncio.run(main()). Функция main() — это асинхронная функция, которая считывает города из файла, 
создает сопрограммы для каждого города для получения данных о погоде с помощью функции openWeather(), 
а затем обрабатывает результаты с помощью функции process_results()."""